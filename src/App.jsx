import { Box, Container, Heading, HStack, Link, Text, Image } from '@chakra-ui/react'
import { useState, useEffect } from 'react'
import './App.css'
import axios from 'axios'


function App() {
  const [nfts, setNfts] = useState([])

  const options = {
    method: "GET",
    url: "https://api.opensea.io/api/v1/assets?format=json",
  };


  const fetchNft = async () => {
    const res = await axios.request(options)

    setNfts(res.data.assets)
  }

  useEffect(() => {
    fetchNft()
  }, [])
  return (
    <Box>
      <NavBar />
      <Container as='main' maxW={"container.xl"} >
        <Box mt={121}>
          <Heading as="h2" textAlign={'center'} m={50} mb={20} > Shoping NFTS</Heading>
          <Box display={"flex"} alignItems="center" justifyContent={"space-evenly"} flexWrap="wrap">
            {nfts.map((el) => (
              <CardNft
                image={el.image_url}
                key={el.id}
                name={el.name}
                link={el.permalink}
                description={el.description}
              />
            ))}
          </Box>
        </Box>
      </Container>
    </Box >
  )
}

export default App


const NavBar = () => {
  return (
    <Box display="flex" alignItems={"center"} justifyContent="space-between" w={"100%"} pos={'fixed'} top={-1} mb={110} bgColor={"black"} h={20}>
      <Container
        display="flex"
        alignItems={"center"}
        justifyContent="space-between"
        color={"white"}
        maxW="container.xl"
      >
        <Heading fontSize={["md", "lg", "x-large"]} as="h1">
          NFT
        </Heading>
        <HStack spacing={[4, 6, 12]}>
          <Link fontSize={["12px", "16px", "18px"]}>Home</Link>
          <Link fontSize={["12px", "16px", "18px"]}>All recepies</Link>
          <Link fontSize={["12px", "16px", "18px"]}>About Us</Link>
        </HStack>
      </Container>
    </Box>
  )
}

const CardNft = ({ name, image, description, link }) => {
  return (
    <Box bgColor={"black"} w={12300} display={'flex'} m={'3%'}>
      <Image src={image} alt={"nft" + "" + name} />
      <Heading as='h3' >
        <Link href={link} isExternal color={'#fff'} fontSize={25} pos={'absolute'} p={10} mb={20}>
          {name}
        </Link>2
      </Heading>
      <Text pt={20} pr={90} color={'#fff'} fontSize={14} fontWeight={900} p='90px 39px'>{description}</Text>
    </Box >
  )
}